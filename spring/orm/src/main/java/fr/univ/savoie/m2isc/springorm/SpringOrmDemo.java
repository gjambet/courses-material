package fr.univ.savoie.m2isc.springorm;

import fr.univ.savoie.m2isc.springorm.dao.LogEntryDAO;
import fr.univ.savoie.m2isc.springorm.entities.LogEntry;
import fr.univ.savoie.m2isc.springorm.services.LogEntryService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringOrmDemo {

    private ClassPathXmlApplicationContext ctx;

    public SpringOrmDemo() {
        ctx = new ClassPathXmlApplicationContext("application-context.xml");
    }

    public SpringOrmDemo run() {

        LogEntryService logEntryService = ctx.getBean(LogEntryService.class);

        logEntryService.log("test log entry");

        return this;
    }


    public SpringOrmDemo verify() {

        LogEntryDAO dao = ctx.getBean(LogEntryDAO.class);

        LogEntry logEntry = dao.findById(1l);

        System.out.println("logEntry : " + logEntry.getPayload() + " from : " + logEntry.getCreator() + " persisted with id : " + logEntry.getId() + " at : " + logEntry.getTimeStamp());

        return this;
    }

}
