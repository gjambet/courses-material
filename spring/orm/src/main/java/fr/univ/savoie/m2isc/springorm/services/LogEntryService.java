package fr.univ.savoie.m2isc.springorm.services;

import fr.univ.savoie.m2isc.springorm.dao.LogEntryDAO;
import fr.univ.savoie.m2isc.springorm.entities.LogEntry;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class LogEntryService {

    @Resource
    private LogEntryDAO dao;

    @Resource
    private UserService userService;

    @Transactional
    public LogEntry log(String message) {
        return dao.save(new LogEntry().creator(userService.getCurrentUser()).payload(message));
    }

}
