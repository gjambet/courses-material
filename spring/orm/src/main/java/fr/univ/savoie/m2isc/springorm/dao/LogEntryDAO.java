package fr.univ.savoie.m2isc.springorm.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.univ.savoie.m2isc.springorm.entities.LogEntry;

import org.springframework.stereotype.Repository;

@Repository
public class LogEntryDAO {

    @PersistenceContext(name = "persistenceUnit")
    private EntityManager em;

    @SuppressWarnings("unchecked")
    public List<LogEntry> findFor(String login) {

        Query query = em.createQuery("from LogEntry as le where le.creator = :creator");
        query.setParameter("creator", login);
        List<LogEntry> l = query.getResultList();

        System.out.println("found " + l.size() + " entries for login : " + login);

        return l;
    }

    public LogEntry findById(Long id) {
        return em.find(LogEntry.class, id);
    }

    public LogEntry save(LogEntry logEntry) {

        LogEntry entry = logEntry;
        if (logEntry.getId() == null) {
            em.persist(logEntry);
        } else {
            entry = em.merge(logEntry);
        }
        em.flush();

        System.out.println("logEntry : " + entry.getPayload() + " persisted with id : " + entry.getId());
        return entry;
    }

}