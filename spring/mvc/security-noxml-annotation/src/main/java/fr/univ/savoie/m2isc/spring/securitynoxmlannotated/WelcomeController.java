package fr.univ.savoie.m2isc.spring.securitynoxmlannotated;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Calendar;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class WelcomeController {

    @RequestMapping(value = "/welcome", method = GET)
    @PreAuthorize("hasRole('ROLE_USER')")
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Spring Security Hello World : " + Calendar.getInstance().getTime());
        return "welcome";
    }

}