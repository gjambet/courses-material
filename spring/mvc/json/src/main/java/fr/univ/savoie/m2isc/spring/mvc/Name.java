package fr.univ.savoie.m2isc.spring.mvc;

import org.codehaus.jackson.annotate.JsonProperty;

public class Name {

    private final String first;
    private final String last;

    public Name(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }
}
