package fr.univ.savoie.m2isc.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
public class DummyController {

    // mvn clean install tomcat7:run
    // http://localhost:8080/json/pipo.json

    @RequestMapping(produces = "application/json", value = "/pipo.json", method = GET)
    public @ResponseBody
    Name pipoBimbo(){
        return new Name("pipo", "bimbo");
    }

}
