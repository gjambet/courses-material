package fr.univ.savoie.m2isc.spring.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DummyController {

    private static final String SUCCESS = "home.jsp";

    @Autowired
    private DummyService dummyService;


    @RequestMapping(value = "/create.do", method = RequestMethod.POST)
    public String create(Model model, @RequestParam String dummyInput) {

        model.addAttribute("dummyString", dummyService.dummy(dummyInput));

        return SUCCESS;
    }


}
