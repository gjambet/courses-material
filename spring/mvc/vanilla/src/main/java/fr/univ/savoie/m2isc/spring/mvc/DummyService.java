package fr.univ.savoie.m2isc.spring.mvc;

import org.springframework.stereotype.Service;

@Service
public class DummyService {

    public String dummy(String message) {
        return "Hello dummy : " + message + " !!!";
    }

}
